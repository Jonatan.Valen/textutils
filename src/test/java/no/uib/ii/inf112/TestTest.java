package no.uib.ii.inf112;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class TestTest {
	TextAligner aligner = new TextAligner() {

		public String center(String text, int width) {
			int extra = (width - text.length()) / 2;
			return " ".repeat(extra) + text + " ".repeat(extra);
		}

		public String flushRight(String text, int width) {
			
			return " ".repeat(width-text.length()) + text;
		}

		public String flushLeft(String text, int width) {
			
			return text + " ".repeat(width-text.length());
		}

		public String justify(String text, int width) {
			String[] words = text.split(" ");
			int num = width-(text.length()-(words.length-1));
			int distance = num/(words.length-1);
			String result = words[0];
			for(int i = 1; i < words.length; i++) {
				result += " ".repeat(distance) + words[i];
			}
			return result;
		}
		
		public String fizzBuzz(int number) {
			if(number % 3 == 0) {
				if(number % 5 == 0) {
					return "fizzbuzz";
				}
				else return "fizz";
			}
			else if(number % 5 == 0) return "buzz";
			else return "Not divisible by 3 or 5";
		}
		};
		
		
	@Test
	void test() {
		fail("Not yet implemented");
	}

	@Test
	void testCenter() {
		assertEquals("  A  ", aligner.center("A", 5));
		assertEquals(" foo ", aligner.center("foo", 5));
		assertEquals("  foo  ", aligner.center("foo", 7));
		assertEquals("   fool   ", aligner.center("fool", 10));

	}
	@Test
	void testFlushRight() {
		assertEquals("    A", aligner.flushRight("A", 5));
		assertEquals("   AB", aligner.flushRight("AB", 5));
	}
	@Test
	void testFlushLeft() {
		assertEquals("A    ", aligner.flushLeft("A", 5));
		assertEquals("B    ", aligner.flushLeft("B", 5));
		assertEquals("AB   ", aligner.flushLeft("AB", 5));
		assertEquals("ABC  ", aligner.flushLeft("ABC", 5));
	}
	@Test
	void testJustify() {
		assertEquals("fee   fie   foo", aligner.justify("fee fie foo", 15));
		assertEquals("faa   fei   fbb", aligner.justify("faa fei fbb", 15));
		assertEquals("faa  fei  fbb", aligner.justify("faa fei fbb", 13));
		assertEquals("faa  fei  fbb", aligner.justify("faa fei fbb", 13));
		assertEquals("faa  fei  fbb  abb", aligner.justify("faa fei fbb abb", 18));
	}
	@Test 
	void testFizzBuzz() {
		assertEquals("fizz", aligner.fizzBuzz(3));
		assertEquals("buzz", aligner.fizzBuzz(5));
		assertEquals("fizzbuzz", aligner.fizzBuzz(15));
		
	}
}
